import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Chart from "react-google-charts";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Navbar from "../../../layouts/navbar";
import CircularProgress from '@material-ui/core/CircularProgress';
import InputAdornment from '@material-ui/core/InputAdornment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import {useDispatch, useSelector} from 'react-redux'
import {getDiagramData, setDiagramData} from "../../../redux/actions/user/percentActions";
import Container from '@material-ui/core/Container';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
  },
  container: {
    maxWidth: '100%',
    marginTop: '10px',
    paddingLeft: '10px',
    paddingRight: '10px',
  },
  list: {
    height: '100%'
  },
  chart: {
    minHeight: '500px'
  },
  table: {
    width: '50%',
  },
  title: {
    flexGrow: 1,
  },
  active: {
    backgroundColor: '#3f51b5',
    color: '#fff',
    cursor: 'not-allowed'
  },
  loader: {
    marginLeft: '45%'
  },
  input: {
    height: '30px'
  }
}));

const Alert = (props) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};

const TableInput = ({col, row, value}) => {
  const [inputVal, setInputVal] = useState('');
  const [errorNotification, setErrorNotification] = useState(false);

  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(() => {
    setInputVal(value)
  }, [value]);

  return (
    <TableCell key={col}>
      <OutlinedInput
        value={inputVal}
        type={"number"}
        variant={"outlined"}
        fullWidth={false}
        className={classes.input}
        onChange={e => {
          const value = e.target.value;

          if( Number(value) < 0 || Number(value) > 100 ){
            setErrorNotification(true);
          }else{
            setInputVal(value)
          }
        }}
        onBlur={() => {
          if (inputVal !== value) {
            dispatch(setDiagramData({
              col,
              row,
              value: inputVal
            }));
          }
        }}
        endAdornment={<InputAdornment position="end">%</InputAdornment>}
      />
      <Snackbar
        open={errorNotification}
        autoHideDuration={2000}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        onClose={() => { setErrorNotification(false) }}
      >
        <Alert onClose={() => { setErrorNotification(false) }} severity={'error'}>
          Value must be between 0 and 100!
        </Alert>
      </Snackbar>
    </TableCell>
  )
};

const TableComponent = ({data}) => {
  const classes = useStyles();
  const cols = !!data ? Object.keys(data[Object.keys(data)[0]]) : [];

  return (
    <Table className={classes.table} size="small" aria-label="a dense table">
      <TableHead>
        <TableRow>
          <TableCell/>
          {cols.map((col) => (
            <TableCell key={col} align={"right"}>{col}</TableCell>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {!!data && Object.entries(data).map(([key, row]) => (
          <TableRow key={key}>
            <TableCell component="th" scope="row">
              {key}
            </TableCell>
            {cols.map((col,index) => (
              <TableInput key={index} col={col} row={key} value={row[col]}/>
            ))}
          </TableRow>
        ))}
      </TableBody>
    </Table>
  )
};

const ChartComponent = ({data}) => {
  const classes = useStyles();
  const rows = !!data ? Object.keys(data) : [];
  let cols = [];

  if (!!data) {
    let dataModified = {};

    rows.map((row) => (
      Object.entries(data[row]).forEach(([key, val]) => {
        if (!dataModified[key]) {
          dataModified[key] = []
        }
        dataModified[key].push({v: val, f: `${val}%`})
      })
    ));

    Object.entries(dataModified).forEach(([key, val]) => {
      cols.push([key, ...val])
    })
  }

  return (
    <Chart
      className={classes.chart}
      chartType="Bar"
      loader={<CircularProgress className={classes.loader}/>}
      data={[
        ['', ...rows],
        ...cols
      ]}
      options={{
        colors: ['#b0120a', '#757ce8', '#ff7961', '#ffab91'],
      }}
    />
  )
};

const HistoryComponent = ({data}) => {
  const historyData = !!data ? data : [];
  const dispatch = useDispatch();
  const classes = useStyles();

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      subheader={
        <ListSubheader component="div" id="nested-list-subheader">
          Edited percents history
        </ListSubheader>
      }
      className={classes.root}>
      {historyData.map((data, index) => {
        const date = new Date(Date.parse(data.date));
        return (
          <ListItem
            button
            className={index === 0 ? classes.active : ''}
            disabled={index === 0}
            key={index}
            onClick={() => {
              dispatch(setDiagramData(data))
            }}
          >
            <ListItemText
              primary={` ${date.toLocaleDateString()}  ${date.toLocaleTimeString()}`}
            />
          </ListItem>
        )
      })}
    </List>
  )
};

const Dashboard = () => {
  const classes = useStyles();
  const data = useSelector(state => state.user.percent);
  const saved = useSelector(state => state.user.percent.saved);
  const [successNotification, setSuccessNotification] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getDiagramData())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setSuccessNotification(saved)
  }, [saved]);

  return (
    <React.Fragment>
      <Navbar/>
      <Container className={classes.container}>
        <Grid container spacing={2}>
          <Grid item lg={10} xs={12} sm={12}>
            <Card>
              <CardContent>
                <Grid container spacing={3}>
                  <Grid item xs={12}>
                    <TableComponent data={data.diagramData}/>
                  </Grid>
                  <Grid item xs={12}>
                    <ChartComponent data={data.diagramData}/>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Grid>
          <Grid item lg={2} xs={12} sm={12}>
            <Card className={classes.list}>
              <CardContent>
                <HistoryComponent data={data.historyData}/>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
        <Snackbar
          open={successNotification}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          autoHideDuration={1000}
          onClose={() => { setSuccessNotification(false) }}
        >
          <Alert onClose={() => { setSuccessNotification(false) }} severity={'success'}>
            Successfully saved!
          </Alert>
        </Snackbar>
      </Container>
    </React.Fragment>
  );
};

export default Dashboard;
