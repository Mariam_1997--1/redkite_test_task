import axios from "axios"
import { getSession } from "../../../utility/session"

const auth = getSession('auth');

const options = {
  headers: {
    'Content-Type': 'application/json',
    'Authorization': !!auth ? auth.accessToken : null
  }
};

export const getDiagramData = () => {
  return dispatch => {
    dispatch({
      type: "GET_USER_DATA_REQUEST",
    });

    axios.get("/api/user/diagramData", options ).then(response => {
      let userData;

      if (response.data) {
        userData = response.data;

        dispatch({
          type: "GET_USER_DATA_SUCCESS",
          payload: userData
        });

        return userData
      }
    }).catch(error => {
      dispatch({
        type: "GET_USER_DATA_FAIL",
        error
      });

      return error
    })
  }
};

export const setDiagramData = ( data ) => {
  return dispatch => {
    dispatch({
      type: "SET_USER_DATA_REQUEST",
    });

    axios.put("/api/user/diagramData", data, options).then(response => {
      let userData;

      if (response.data) {
        userData = response.data;

        dispatch({
          type: "SET_USER_DATA_SUCCESS",
          payload: userData
        })
      }
    }).catch(error => {
      dispatch({
        type: "SET_USER_DATA_FAIL",
        error
      })
    })
  }
};


