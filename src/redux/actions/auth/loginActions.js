import history  from "../../../history"
import axios from "axios"
import {
  saveSession,
  destroySession
} from "../../../utility/session";

export const loginUser = user => {
  return dispatch => {
    axios
      .post(`/api/authenticate/login/user`, {
        email: user.email,
        password: user.password
      })
      .then(response => {
        let user;

        if (response.data) {
          user = response.data;

          dispatch({
            type: "LOGIN_USER",
            payload: { user }
          });

          saveSession('auth',user);

          history.push("/")
        }
      })

  }
};

export const logoutUser= () => {
  return dispatch => {
    dispatch({ type: "LOGOUT_USER", payload: {} });
    destroySession();
    history.push("/login")
  }
};

