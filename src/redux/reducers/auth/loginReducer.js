import {
  getSession
} from "../../../utility/session";

const auth = getSession('auth');
const INIT_STATE = {
  ...auth,
};

export const login = (state = INIT_STATE, action) => {
  switch (action.type) {
    case "LOGIN_USER": {
      return { ...state, user: action.payload.user }
    }
    case "LOGOUT_USER": {
      return { ...state, user: action.payload }
    }
    default: {
      return state
    }
  }
};
