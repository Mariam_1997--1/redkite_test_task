import { combineReducers } from "redux"
import { percent } from "./percentReducer"

const userReducers = combineReducers({
  percent,
});

export default userReducers
