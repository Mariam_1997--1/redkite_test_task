export const percent = (state = {}, action) => {
  switch (action.type) {
    case "GET_USER_DATA_REQUEST": {
      return {
        ...state,
      }
    }
    case "GET_USER_DATA_SUCCESS": {
      return {
        ...state,
        ...action.payload.userData
      }
    }
    case "GET_USER_DATA_FAIL": {
      return {
        ...state,
        error: action.error
      }
    }
    case "SET_USER_DATA_REQUEST": {
      return {
        ...state,
        saved: false
      }
    }
    case "SET_USER_DATA_SUCCESS": {
      return {
        ...state,
        ...action.payload.userData,
        saved: true
      }
    }
    case "SET_USER_DATA_FAIL": {
      return {
        ...state,
        error: action.error
      }
    }
    default: {
      return state
    }
  }
};
