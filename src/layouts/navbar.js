import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { logoutUser } from "../redux/actions/auth/loginActions";
import {connect} from "react-redux";

const useStyles = makeStyles(() => ({
  title: {
    flexGrow: 1,
  },
}));


const Navbar = (props) => {
  const classes = useStyles();
  return (
      <AppBar position="static">
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
            </Typography>
            <Button color="inherit" onClick={() => props.logoutUser()}>Logout</Button>
          </Toolbar>
        </AppBar>
      </AppBar>
  );
};

export default  connect(null, { logoutUser })(Navbar);
