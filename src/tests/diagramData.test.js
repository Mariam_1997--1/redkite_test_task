import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import * as actions from '../redux/actions/user/percentActions';

const middleware = [thunk];
const mockStore = configureMockStore(middleware);
const mock = new MockAdapter(axios);
const store = mockStore();

describe('getDiagramData actions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  it('dispatches GET_USER_DATA_SUCCESS after a successfully API request', () => {
    mock.onGet('/api/user/diagramData').reply(200, {
      response: {
        userData: {
          userId: 1,
          diagramData: {},
          historyData: []
        }
      }
    });
    store.dispatch(actions.getDiagramData())
    //   .then(() => {
    //   let expectedActions = [
    //     { type: 'GET_USER_DATA_REQUEST' },
    //     {
    //       type: 'GET_USER_DATA_SUCCESS',
    //       payload: {userData: { userId:1, diagramData: {}, historyData: []}} }
    //     }
    //   ];
    //   expect(store.getActions()).toEqual(expectedActions)
    // });
  });

  it('dispatches GET_USER_DATA_FAIL after a failed  API request', () => {
    mock.onGet('/api/user/diagramData').reply(400, {error: {message: 'error message'}});
    store.dispatch(actions.getDiagramData())
    //   .then(() => {
    //   let expectedActions = [
    //     { type: 'GET_USER_DATA_REQUEST' },
    //     {
    //       type: 'GET_USER_DATA_FAIL',
    //       payload: { error: { message: 'error message' } }
    //     }
    //   ];
    //   expect(store.getActions()).toEqual(expectedActions)
    // });
  });
});

describe('setDiagramData actions', () => {
  beforeEach(() => {
    store.clearActions();
  });
  it('dispatches SET_USER_DATA_SUCCESS after a successfully API request', () => {
    mock.onPost('/api/user/diagramData').reply(201, {
      response: {
        userData: {
          userId: 1,
          diagramData: {},
          historyData: []
        }
      }
    });
    store.dispatch(actions.setDiagramData())
    //   .then(() => {
    //   let expectedActions = [
    //     { type: 'SET_USER_DATA_REQUEST' },
    //     { type: 'SET_USER_DATA_SUCCESS', payload: {userData: { userId:1, diagramData: {}, historyData: []}} }
    //   ];
    //   expect(store.getActions()).toEqual(expectedActions)
    // });
  });

  it('dispatches SET_USER_DATA_FAIL after a failed API request', () => {
    mock.onPost('/api/user/diagramData').reply(400, {error: {message: 'error message'}});
    store.dispatch(actions.setDiagramData())
    //   .then(() => {
    //   let expectedActions = [
    //     { type: 'SET_USER_DATA_REQUEST' },
    //     {
    //       type: 'SET_USER_DATA_FAIL',
    //       payload: { error: { message: 'error message' } }
    //     }
    //   ];
    //   expect(store.getActions()).toEqual(expectedActions)
    // });
  });
});
