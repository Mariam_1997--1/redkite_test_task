import SessionStorage from '../services/SessionStorage';

export const saveSession = (key , data) => {
  const options = {
    path: '/',
    expires: new Date( Date.now()),
  };
  SessionStorage.set(key, data, options);
};

export const getSession = (key) => {
  return SessionStorage.get(key);
};

export const destroySession = () => {
  const options = {
    path: '/'
  };
  SessionStorage.remove('auth', options);
};
