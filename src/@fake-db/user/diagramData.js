import mock from "../mock"
import jwt from "jsonwebtoken"
import { users } from "./userData"
import {getSession, saveSession} from "../../utility/session";

const jwtConfig = {
  "secret"   : "dd5f3089-40c3-403d-af14-d0c228b05cb4",
};

const getRandomPercent = (min=0, max=100) => {
  return Math.round(Math.random() * (max - min) + min);
};

const generateData = () => {
  const cols = [ 10, 20, 30, 40 ];
  const rows = [ 'a', 'b', 'c', 'd' ];
  let data = [];

  users.map( (user) => (
    data.push({
      userID: user.id,
      diagramData: (() => {
        const diagramData = {};

        rows.map( row => (

          diagramData[row] = (() => {
            let data = {};

            cols.map( row => (
              data[row] = getRandomPercent()
            ));

            return data;
          })()

        ));

        return diagramData
      })(),
      historyData: (() => {
        const historyData = [];

        for( let i=0; i<Math.floor(Math.random() * 10); i++ ){
          historyData.push({
            col: cols[Math.floor(Math.random() * cols.length)],
            row: rows[Math.floor(Math.random() * rows.length)],
            value: getRandomPercent(),
            date: new Date()
          })
        }

        return historyData
      })()
    })
  ));

  return data;
};

let data = getSession('diagramData');
if( !data ) {
  data = generateData();
  saveSession('diagramData',data);
}


// GET : DIAGRAM DATA BY USER
mock.onGet("/api/user/diagramData").reply(request => {
  const accessToken = request.headers.Authorization;

  try {
    const {id} = jwt.verify(accessToken, jwtConfig.secret);

    let userData = Object.assign({}, data.find(data => data.userID === id));
    //
    const response = {
      userData,
    };

    return [200, response];

  } catch (e){
    const error = "Invalid access token";
    return [401, {error}]
  }
});

// SET: DIAGRAM DATA BY USER
mock.onPut("/api/user/diagramData").reply(request => {
  const accessToken = request.headers.Authorization;
  const requestData = JSON.parse(request.data);

  try {
    const {id} = jwt.verify(accessToken, jwtConfig.secret);
    const { row, col, value } = requestData;

    let userData = Object.assign({}, data.find(data => data.userID === id));

    userData.historyData.unshift({
      ...requestData,
      date:new Date()
    });

    if(userData.historyData.length>11){
      userData.historyData.pop();
    }
    userData.diagramData[row][col] = value;

    saveSession('diagramData',data);

    const response = {
      userData
    };

    return [200, response];

  } catch (e){
    const error = "Invalid access token";
    return [401, {error}]
  }
});
