import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { useSelector } from "react-redux";
import Login from './views/pages/authentication/login';
import Dashboard from './views/pages/user/dashboard';


const PrivateRoute = ({component: Component, auth }) => (
  <Route render={props => auth
    ? <Component auth={auth} {...props} />
    : <Redirect to={{pathname:'/login'}} />
  }
  />
);

const Router = () => {
  const auth = useSelector(state => !!state.auth.login.user);

  return (
    <BrowserRouter>
        <Switch>
          <Route exact  path="/login"  component={Login} />
          <PrivateRoute
            path='/'
            auth={auth}
            component={Dashboard}
          />
        </Switch>
    </BrowserRouter>
  )
};

export default Router;
